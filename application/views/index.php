<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
    <title>AT&T Internet, wireless</title>
  </head>
  <body>
      <nav class="navbar navbar-expand-lg navbar-light att-navbar">
        <div class="container">
          <a class="navbar-brand" href="#"><img src="images/logo.png" alt="Logo"></a>
          <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-lines"></span>
            <span class="navbar-toggler-lines"></span>
            <span class="navbar-toggler-lines"></span>
          </button>

          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ml-auto att-menu">
              <li class="nav-item active">
                <a class="nav-link" href="#"><img src="images/Internet.png" alt="Internet"><br>Internet</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <img src="images/TV.png" alt="TV"><br>TV 
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">AT&T TV</a>
                  <a class="dropdown-item" href="#">DIRECTV</a>
                  <a class="dropdown-item" href="#">U-verse TV</a>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link" href="#"><img src="images/Wireless.png" alt="Wireless"><br>Wireless</a>
              </li>
              <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <img src="images/Bundles.png" alt="Wireless"><br>Bundles
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                  <a class="dropdown-item" href="#">AT&T TV</a>
                  <a class="dropdown-item" href="#">DIRECTV</a>
                </div>
              </li>
            </ul>
          </div>
        </div>
      </nav>

      <!-- Navbar End here -->
      <div class="container-fluid main-body">
        <div class="row mob-red-bar d-block d-md-none">
          <div class="container">
            <p class="m-0 text-center"><b>Hurry! Get AT&amp;T Visa<sup>® </sup>Reward Cards with Order!</b><br> <span style="font-family: 'ATT Aleck Sans Reg'; font-weight: normal;"><a style="color: #ffffff;" href="#">Learn More</a></span></p>
          </div>
        </div>
        <div class="row timer-bar d-none d-md-block">
          <div class="container">
            <div class="row">
            <div class="col-md-6 text-md-left text-sm-center timer-bar-text">Call in 06 hrs : 04 min : 26 sec to Save!</div>
            <div class="col-md-6 text-md-right text-sm-center timer-bar-text">CALL TODAY 1-833-887-3016</div>
            </div>
          </div>
        </div>
      <div class="row mobile-cover">
        <img src="images/ATT-Home-Tablet-Image-480x290.jpg" alt="image">
      </div>
      <div class="row att-cover">
        <div class="container">
          <h1>
            MORE OF WHAT MATTERS.<br>
            <span style="font-family: 'ATT Aleck Sans Bold',Helvetica, Arial, Lucida, sans-serif;">THAT’S OUR THING.</span>
          </h1>
            <div class="cover-text">
              <p class="att-text-4">Entertainment the way it was meant to be with<br> 
              <a href="#">AT&T Internet</a>, <a href="#">AT&T wireless</a>, and <a href="#">AT&T TV</a> plans.</p>
              <p class="att-text-5">AT&T Internet 1000</p>
              <p class="att-text-6"><sup>$</sup>39<span style="font-size: 25px;vertical-align: super;"><sup>99</sup></span></p>
              <p class="att-text-inner">/mo. for 12 mos. when bundled plus taxes & equip. fee</p>
              <p class="att-text-7">Other qualifying svc. (min. $19/mo.) & combined bill req’d. $10/mo. equip. fee applies. Incl. unlimited internet data allowance ($30 value). See offer details.
              </p>
            </div>
        </div>
      </div>  
      <div class="row get-reward">
        <div class="container text-center">
          <h1 class="att-text-1">GET A <sup>$</sup>250 AT&T VISA<sup>®</sup> REWARD CARD</h1>
          <p class="att-text-2">when you bundle AT&T Internet & AT&T TV</p>
          <p class="att-text-3">w/24-mo. AT&T TV & 12-mo. Internet agmts. Internet plans 25M+. Redemption req’d. Ends 3/31/20. See offer details.</p>
        </div>
      </div>
      <div class="row plans-card">
          <div class="container margin-card-min">
            <div class="row">
              <!-- card start -->
              <div class="col-md-6 col-sm-1 col-lg-3">
                <div class="plan-card">
                  <div class="card-heading">
                    AT&T INTERNET
                  </div>
                  <div class="card-title text-center">
                    <img src="images/Internet-Icon.png" alt="Internet icon"><br>
                    Internet 1000 <br>powered by AT&T Fiber<sup><sub>sm</sub></sup>
                  </div>
                  <div class="card-value">
                    <small><sup>$</sup></small>39<span style="font-size: 25px;vertical-align: super"><sup>99</sup></span>
                  </div>
                  <div class="card-value-mon">
                    /mo.
                  </div>
                  <div class="para">
                    <p>
                      for 12 mos. when bundled plus taxes & equipment fee. Other qualifying svc. (min. $19/mo.) & combined bill req’d. $10/mo. equip. fee applies. Incl. unlimited internet data allowance ($30 value).
                    </p>
                    <a href="#">See offer details</a>
                  </div>
                  <div class="see-more">
                    <a href="#" class="att-button">See More</a>
                  </div>
                  <img src="images/internet2.jpg" alt="Image" width="100%">
                </div>
              </div>
              <!-- card end -->

              <!-- card start -->
              <div class="col-md-6 col-sm-1 col-lg-3">
                <div class="plan-card">
                  <div class="card-heading">
                    AT&T WIRELESS
                  </div>
                  <div class="card-title text-center">
                    <img src="images/Wireless-Icon.png" alt="icon"><br>
                    AT&T Unlimited<br>Starter<sup><sub>sm</sub></sup>
                  </div>
                  <div class="card-value">
                    <small><sup>$</sup></small>35
                  </div>
                  <div class="card-value-mon ml-0">
                    /mo.
                  </div>
                  <div class="para">
                    <p>
                      per line for 4 lines plus taxes. $140/mo. for 4 lines. Prices after discount with AutoPay and Paperless bill. AT&T may temporarily slow data speeds when the network is busy.
                    </p>
                    <a href="#">See offer details</a>
                  </div>
                  <div class="see-more">
                    <a href="#" class="att-button">See More</a>
                  </div>
                  <img src="images/wireless2.jpg" alt="Image" width="100%">
                </div>
              </div>
              <!-- card end -->

              <!-- card start -->
              <div class="col-md-6 col-sm-1 col-lg-3">
                <div class="plan-card">
                  <div class="card-heading">
                    AT&T TV
                  </div>
                  <div class="card-title text-center">
                    <img src="images/Directv-Icon.png" alt="icon"><br>
                    AT&T TV<br>Entertainment
                  </div>
                  <div class="card-value">
                    <small><sup>$</sup></small>39<span style="font-size: 25px;vertical-align: super"><sup>99</sup></span>
                  </div>
                  <div class="card-value-mon">
                    /mo.
                  </div>
                  <div class="para">
                    <p>
                      or 12 mos. plus taxes w/24-mo. TV agmt. & AT&T Internet (min. $39.99/mo. plus taxes and fees). TV price higher in 2nd year.
                    </p>
                    <a href="#">See offer details</a>
                  </div>
                  <div class="see-more">
                    <a href="#" class="att-button">See More</a>
                  </div>
                  <img src="images/DIRECTV.jpg" alt="Image" width="100%">
                </div>
              </div>
              <!-- card end -->

              <!-- card start -->
              <div class="col-md-6 col-sm-1 col-lg-3">
                <div class="plan-card">
                  <div class="card-heading">
                    BUNDLES
                  </div>
                  <div class="card-title text-center">
                    <img src="images/Bundle-Icon.png" alt="Internet icon"><br>
                    AT&T TV<br>Entertainment<br>+<br>AT&T Internet 1000
                  </div>
                  <div class="card-value">
                    <small><sup>$</sup></small>79<span style="font-size: 25px;vertical-align: super"><sup>98</sup></span>
                  </div>
                  <div class="card-value-mon">
                    /mo.
                  </div>
                  <div class="para">
                    <p>
                      for 12 mos. plus taxes & internet equip. fee w/ 24-mo. TV agmt. Prices higher in 2nd year.* $10/mo. internet equip. fee applies. Incl. unlimited data allowance ($30 value).†.
                    </p>
                    <a href="#">See offer details</a>
                  </div>
                  <div class="see-more">
                    <a href="#" class="att-button">See More</a>
                  </div>
                  <img src="images/ATT_TV_Internet.png" alt="Image" width="100%" class="pr-2">
                </div>
              </div>
              <!-- card end -->
            </div>
            <div class="cards-disc text-center">
              <p><sup>††</sup>Internet speed claims represent maximum network service capability speeds and based on wired connection to gateway. Actual customer speeds may vary based on a number of factors and are not guaranteed. Download speeds are typically up to 940 Mbps due to overhead capacity reserved to deliver the data. For more information, go to www.att.com/speed101.</p>
              
              <p>
              <b>*$19.95 ACTIVATION, EARLY TERMINATION FEES ($15/MO. FOR TV) FOR EACH MONTH REMAINING ON AGMT., EQUIPMENT NON-RETURN & ADD’L. FEES APPLY.</b> Price incl. ENTERTAINMENT AT&T TV Pkg., 1 AT&T TV device & is after$10/mo. bundle discount on TV for up to 12 mos. <b>Pay $49.99/mo. plus taxes and fees on TV until discount starts w/in 3 mos.</b> New approved residential customers, excluding DIRECTV and U-verse TV customers. Restr’s. apply.
              </p>
            </div>
          </div>
      </div>


      <div class="row help-section">
        <div class="container help-tabs">
          <div class="row">
            <div class="col-lg-5 col-md-12"></div>
            <div class="col-lg-7 col-md-12 att-tabs">
              <h1>
                WE’RE HERE TO HELP!<br>
                <span style="font-family: 'ATT Aleck Sans Bold',Helvetica, Arial, Lucida, sans-serif;">FREQUENTLY ASKED QUESTIONS:</span>
              </h1>

              <div class="accordion" id="accordionExample">
                <div class="card">
                  <div class="card-header" id="headingOne">
                    <h2 class="mb-0">
                      <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                        Why do I need an AT&T account?
                      </button>
                    </h2>
                  </div>

                  <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                    <div class="card-body">
                      Your AT&T account makes it easier to pay all your AT&T bills for any service, whether business, residential, wireless, or even wearables. If you have any AT&T device, you already have an account, and all you need to do to use it is activate it.
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingTwo">
                    <h2 class="mb-0">
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                        How do I create an AT&T account?
                      </button>
                    </h2>
                  </div>
                  <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                    <div class="card-body">
                      All you have to do to create your AT&T account is go to the myAT&T sign-in page with your billing zip code and either your AT&T phone number or account number. You may even already have an account, if you have other AT&T devices or services. From there, just follow the prompts. Your AT&T account allows you to view and pay your bill online, combine your services into one convenient monthly bill, manage your accounts and even sign up for paperless billing.
                    </div>
                  </div>
                </div>
                <div class="card">
                  <div class="card-header" id="headingThree">
                    <h2 class="mb-0">
                      <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                        What services does AT&T offer businesses?
                      </button>
                    </h2>
                  </div>
                  <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                    <div class="card-body">
                      AT&T is known for fast, reliable Internet and telephone services, but we offer a variety of services to support your business, no matter how small or large. Voice and collaboration services can help streamline meetings with clients, colleagues and suppliers, while our 5G is available for businesses in over 20 cities and counting. We also offer cloud storage and cybersecurity so your clients can trust that their data is safe. AT&T is committed to fulfilling the demands of today's business technology.
                    </div>
                  </div>
                </div>
              </div>

              <a href="#" class="att-button">More FAQ</a>
            </div>
          </div>
        </div>
      </div>

      <div class="row att-footer">
        <div class="container">
          <div class="row">
            <div class="col-md-9 footer-content">
              <p class="footer-menu">
                <a href="#">Privacy Policy</a> 
                <span style="padding: 0 5px;">|</span> 
                <a href="#">Terms &amp; Conditions</a> 
                <span style="padding: 0 5px;">|</span> 
                <a href="#">Do Not Sell My Information</a>
              </p>
              <p><span style="font-size: 12px;">All calls with ATT-Bundles.com are monitored and recorded for quality assurance and training purposes. Geographic and service restrictions apply to AT&amp;T services. © 2020 AT&amp;T Intellectual Property. All Rights Reserved. AT&amp;T, Globe logo, DIRECTV, and all other DIRECTV marks contained herein are trademarks of AT&amp;T Intellectual Property and/or AT&amp;T affiliated companies. All other marks are the property of their respective owners.</span></p>

              <p><span style="font-size: 12px;">COPYRIGHT © 2020 ATT-Bundles.com. All rights reserved.</span></p>
            </div>
            <div class="col-md-3 footer-logo text-center text-md-right">
              <img src="images/logo-footer-att.png" alt="White-logo" width="130px" class="text-center text-md-right mb-3"><br>
                <span style="font-size: 16px;">Bundle Your Home</span><br>
                <span style="font-size: 12px;">AT&amp;T Authorized Retailer</span>
            </div>
          </div>
        </div>
      </div>


      </div>

      <div class="bottom-number">
        <div class="container text-center">
          <a href="#"><img src="images/Call-Now-Icon.png" alt="Call-Now-Icon" class="mr-3"> Call <span class="d-none d-md-inline-block">& get set up today!</span> 1-833-887-3016</a>
        </div>
      </div>

    <script src="js/jquery-3.4.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
  </body>
</html>